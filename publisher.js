const amqp = require('amqplib')
const queue = 'messages';


exports.publisher = async function(message) {
    const connection = await amqp.connect('amqp://localhost')
    const channel = await connection.createChannel()
    await channel.assertQueue(queue)
    const data = {
        id: Math.random().toString(32).slice(0, 10),
        text: message
    }
    const sent = await channel.sendToQueue(
        queue,
        Buffer.from(JSON.stringify(data))
    )

   return sent;
}

exports.publisher('Hello 1')
exports.publisher('Hello 2')