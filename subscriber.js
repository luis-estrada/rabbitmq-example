const amqp = require('amqplib')
const queue = 'messages';


exports.subscriber = async function() {
    const connection = await amqp.connect('amqp://localhost')
    const channel = await connection.createChannel()
    await channel.assertQueue(queue)
    channel.consume(queue, (message) => {
        const content = JSON.parse(message.content.toString())
        console.log(`Received message from "${queue}" queue`)
        console.log(content)
        channel.ack(message)
    })
}

exports.subscriber()
